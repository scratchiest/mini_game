$(document).ready(() => {
    let scores, roundScore, activePlayer, gamePlaying, player_1, player_2, global_score, plname1, plname2;

    // Modal Content
    $('#startModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    })

    $('#playGame').click(() => {
        $('#startModal').modal('hide');
        player_1 = $('#player_1').val();
        player_2 = $('#player_2').val();
        global_score = $('#global_score').val();

        app.init();
    })

    let app = {
        init: () => {
            scores = [0,    0];
            activePlayer = 0;
            roundScore = 0;
            gamePlaying = true;
            document.getElementById("dice-1").style.display = "none";
            document.getElementById("dice-2").style.display = "none";
            document.getElementById("score-0").textContent = '0';
            document.getElementById("score-1").textContent = '0';
            document.getElementById("current-0").textContent = '0';
            document.getElementById("current-1").textContent = '0';
            document.getElementById("name-0").textContent = player_1 === '' ? "Player 1" : player_1;
            document.getElementById("name-1").textContent = player_2 === '' ? "Player 2" : player_2;
            plname1 = document.getElementById("name-0").textContent;
            plname2 = document.getElementById("name-1").textContent;
            document.getElementById("global-score").value = global_score === '' ? 100 : global_score;
            document.querySelector(".player-0-panel").classList.remove("winner");
            document.querySelector(".player-1-panel").classList.remove("winner");
            document.querySelector(".player-0-panel").classList.remove("active");
            document.querySelector(".player-1-panel").classList.remove("active");
            document.querySelector(".player-0-panel").classList.add("active");
        },
    
        nextPlayer: () => {
            activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
            roundScore = 0;
            document.getElementById("current-0").textContent = "0";
            document.getElementById("current-1").textContent = "0";
            document.querySelector(".player-0-panel").classList.toggle("active");
            document.querySelector(".player-1-panel").classList.toggle("active");
            document.getElementById("dice-1").style.display = "none";
            document.getElementById("dice-2").style.display = "none";
            $.notify({
                // options
                message: activePlayer === 0 ? $('#name-0').text() + "'s turn." : $('#name-1').text() + "'s turn." 
            },{
                // settings
                element: 'body',
                position: null,
                type: "info",
                allow_dismiss: true,
                newest_on_top: false,
                showProgressbar: false,
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: 20,
                spacing: 10,
                z_index: 1031,
                delay: 1000,
                timer: 1000,
                url_target: '_blank',
                mouse_over: null,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                onShow: null,
                onShown: null,
                onClose: null,
                onClosed: null,
                icon_type: 'class',
                template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>' 
            });
        },
    
        holdScore: () => {
            if (gamePlaying) {
                scores[activePlayer] += roundScore;
                
                document.querySelector("#score-" + activePlayer).textContent = scores[activePlayer];
                
                if (scores[activePlayer] >= global_score) {
                    document.querySelector("#name-" + activePlayer).textContent = 'Winner!';
                    document.getElementById("dice-1").style.display = "none";
                    document.getElementById("dice-2").style.display = "none";
                    document.querySelector(".player-" + activePlayer + "-panel").classList.add("winner");
                    document.querySelector(".player-" + activePlayer + "-panel").classList.remove("active");
                    gamePlaying = false;

                    $('#winner').html(activePlayer === 0 ? plname1 + ' WINS!': plname2 + ' WINS!');
                    $('#extra').html(scores[activePlayer]);
                    $('#winModal').modal('show');
                    $('#endGame').click(() => {
                        $('#winModal').modal('hide');
                    })
                }
                else {
                    app.nextPlayer();
                }
            }
        },
    
        rollDice: () => {
            if (gamePlaying) {
                let dice1 = Math.floor(Math.random() * 6) + 1;
                let dice2 = Math.floor(Math.random() * 6) + 1;
        
                document.getElementById("dice-1").style.display = "block";
                document.getElementById("dice-2").style.display = "block";
                document.getElementById("dice-1").src = "assets/img/dice-" + dice1 + ".png";
                document.getElementById("dice-1").src = "assets/img/dice-" + dice2 + ".png";
        
                if (dice1 !== 1 && dice2 !== 1) {
                    roundScore += dice1 + dice2;
                    document.querySelector("#current-" + activePlayer).textContent = roundScore;
                }
                else {
                    app.nextPlayer();
                }   
            }
        },

        announceWinner: () => {
            $('#winModal').modal('show');
        }
    }
    
    document.querySelector('.btn-new').addEventListener('click', app.init);
    document.querySelector('.btn-hold').addEventListener('click', app.holdScore);
    document.querySelector('.btn-roll').addEventListener('click', app.rollDice);
});